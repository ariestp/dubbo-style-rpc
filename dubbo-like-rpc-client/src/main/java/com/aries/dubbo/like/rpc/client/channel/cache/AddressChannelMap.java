package com.aries.dubbo.like.rpc.client.channel.cache;

import com.aries.dubbo.like.rpc.client.bootstrap.ClientBootStrap;
import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/23
 * Description: 缓存了address和channel
 */
public class AddressChannelMap {
    //key:  host:port      value:  channel
    private static Map<String, Channel> ADDRESS_CHANNEL_MAP = new ConcurrentHashMap<>();

    /**
     * 将address作为key，channel作为value put进该map
     *
     * @param addr
     * @param channel
     */
    public static void add(String addr, Channel channel) {
        ADDRESS_CHANNEL_MAP.put(addr, channel);
    }

    /**
     * 根据address从map中获取channel
     *
     * @param addr
     * @return
     */
    public static Channel get(String addr) {
        //如果当前map没有存储该host:port对应的channel，先连接，再返回。
        if (ADDRESS_CHANNEL_MAP.get(addr) == null) {
            CountDownLatch countDownLatch = new CountDownLatch(1);

            new Thread(() -> {
                String[] ipAndPort = addr.split(":");
                try {
                    ClientBootStrap.connect(ipAndPort[0], Integer.valueOf(ipAndPort[1]), countDownLatch, Thread.currentThread());
                } catch (Exception e) {
                    throw new RuntimeException("connect to remote server:" + addr + " failed!");
                }
            }, "a-thread-created-for-netty-connection").start();

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return ADDRESS_CHANNEL_MAP.get(addr);
    }
}
