package com.aries.rpc.spring.starter.service.helper;

import com.aries.dubbo.like.rpc.client.bootstrap.ClientBootStrap;
import com.aries.dubbo.like.rpc.server.bootstrap.RpcServerBootStrap;
import com.aries.dubbo.like.rpc.service.util.discover.zk.ZookeeperInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/24
 * Description: 在spring 容器关闭后，调用此方法。主要为了销毁zk连接，杀死rpc server和client建立的连接以及线程。
 */
@Component
@Slf4j
public class ServiceDestroy {
    @PreDestroy
    public void destroy() {
        log.info("do destroy method ");
        //关闭zookeeper连接
        ZookeeperInstance.closeZkConnect();
        log.info("closed zookeeper connection.");
        //关闭netty服务器
        RpcServerBootStrap.shutDown();
        log.info("rpc server has shutdown.");
        //关闭新建客户端连接的线程，EventLoopGroup线程池和channel

        log.info("rpc client channel and thread has disconnected and interrupted.");
        ClientBootStrap.shutDown();

    }
}
