//package com.aries.rpc.spring.starter.service.helper;
//
//import com.aries.dubbo.like.rpc.common.annotations.EnableRpcProvider;
//import com.aries.dubbo.like.rpc.common.context.RpcServerContext;
//import com.aries.dubbo.like.rpc.common.util.ClassUtil;
//import com.aries.rpc.spring.starter.config.RpcConfiguration;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
//import org.springframework.beans.factory.support.BeanDefinitionBuilder;
//import org.springframework.beans.factory.support.BeanDefinitionRegistry;
//import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
//import org.springframework.beans.factory.support.GenericBeanDefinition;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//import java.util.Set;
//
///**
// * Created with IntelliJ IDEA.
// * Author: aries
// * Date: 2018/8/23
// * Description:
// */
//@Component
//@EnableConfigurationProperties(RpcConfiguration.class)
//@ConditionalOnClass(EnableRpcProvider.class)
//@Slf4j
//public class RpcServerApplicationAware implements ApplicationContextAware, BeanDefinitionRegistryPostProcessor {
//
//    private ApplicationContext applicationContext;
//    private Set<Class<?>> classSet;
//
//    @Override
//    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
//        Set<Class<?>> classSet = ClassUtil.getRpcProviderClassSet();
//        this.classSet = classSet;
//        if (!classSet.isEmpty())
//            classSet.forEach(clz -> {
//                Class<?> clzInterface = clz.getInterfaces()[0];
//                BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(clzInterface);
//                GenericBeanDefinition definition = (GenericBeanDefinition) builder.getRawBeanDefinition();
////                definition.getPropertyValues().add("interfaceClass", definition.getBeanClassName());
////                definition.setBeanClass(clz.getClass());
//                definition.setBeanClassName(clz.getName());
//                definition.setAutowireMode(GenericBeanDefinition.AUTOWIRE_BY_TYPE);
//                int index = clzInterface.getName().lastIndexOf(".");
//                String beanName = clzInterface.getName().substring(index + 1);
//                log.info("register bean " + clzInterface.getName() + " named " + beanName);
//                beanDefinitionRegistry.registerBeanDefinition(beanName, definition);
//            });
//    }
//
//    @Override
//    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
//        Set<Class<?>> classSet = this.classSet;
//        classSet.forEach(c -> {
//            Object object = factory.getBean(c);
//            log.info("add bean and bean class name into rpcServerContext. bean: " + c.getInterfaces()[0].getName());
//            RpcServerContext.add(c.getInterfaces()[0].getName(), object);
//        });
//
//    }
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//        Map<String, Object> map = applicationContext.getBeansWithAnnotation(SpringBootApplication.class);
//        Object object = map.values().toArray()[0];
//        Class<?> clz = object.getClass();
//        if (clz.isAnnotationPresent(ComponentScan.class)) {
//            ComponentScan componentScan = clz.getDeclaredAnnotation(ComponentScan.class);
//            ClassUtil.setBasePath(componentScan.value()[0]);
//
//        } else {
//            throw new RuntimeException("please annotate spring boot application class with ComponentScan and config path.");
//        }
//    }
//
//
//}
