package com.aries.rpc.spring.starter.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/23
 * Description:
 */
public class RpcBeanFactory<T> implements FactoryBean<T> {

    @Getter
    @Setter
    private Class<T> clz;

    @Override
    public T getObject() throws Exception {
        return clz.newInstance();
    }

    @Override
    public Class<?> getObjectType() {
        return this.clz;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
