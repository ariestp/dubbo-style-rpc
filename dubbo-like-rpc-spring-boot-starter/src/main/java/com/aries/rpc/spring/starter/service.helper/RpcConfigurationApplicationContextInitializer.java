package com.aries.rpc.spring.starter.service.helper;

import com.aries.rpc.spring.starter.domain.AnnotationBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/25
 * Description:
 */
@Slf4j
public class RpcConfigurationApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        Environment environment = configurableApplicationContext.getEnvironment();
        String scan = environment.getProperty("dubbo-style-rpc.config.base-path");
        try {
            AnnotationBean annotationBean = AnnotationBean.class.newInstance();
            annotationBean.setBasePath(scan);
            annotationBean.setApplicationContext(configurableApplicationContext);
            configurableApplicationContext.addBeanFactoryPostProcessor(annotationBean);
            configurableApplicationContext.getBeanFactory().addBeanPostProcessor(annotationBean);
            configurableApplicationContext.getBeanFactory().registerSingleton("annotationBean", annotationBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
