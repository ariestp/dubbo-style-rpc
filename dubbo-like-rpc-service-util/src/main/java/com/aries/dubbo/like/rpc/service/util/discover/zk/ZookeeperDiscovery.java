package com.aries.dubbo.like.rpc.service.util.discover.zk;

import lombok.extern.slf4j.Slf4j;
import org.I0Itec.zkclient.ZkClient;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/24
 * Description: 拉取zk的注册信息。
 */
@Slf4j
public class ZookeeperDiscovery {
    /**
     * zk 注册信息的本地缓存   key:class name,value: ip and port
     */
    private volatile static Map<String, List<String>> zkRegisterData;
    private static ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

    /**
     * client启动时调用。
     */
    public static void pullZkData() {

        service.scheduleWithFixedDelay(() -> {
            ZkClient zkClient = ZookeeperInstance.getZkClient();
            zkRegisterData = zkClient.readData(ZookeeperRegistry.getBasePath());
            log.info("pull data from zookeeper ");
        }, 10, 30, TimeUnit.SECONDS);
    }

    public static Map<String, List<String>> getRegistryData() {
        return zkRegisterData;
    }
}
