package com.aries.dubbo.like.rpc.service.util.discover.zk;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.serialize.SerializableSerializer;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/24
 * Description:
 */
public class ZookeeperInstance {

    private static volatile ZkClient zkClient;

    public static void setZkClient(String address) {
        zkClient = new ZkClient(address, 20000,
                60000, new SerializableSerializer());
    }

    public static ZkClient getZkClient() {
        return zkClient;
    }

    public static void closeZkConnect() {
        zkClient.close();
    }
}
