# dubbo-style-rpc

#### 项目介绍

1. 造了一个类似dubbo的 rpc框架，以及spring boot start。
2. 支持同步和异步请求，同步请求直接接口调用即可。
3. 异步请求请查看：com.aries.dubbo.like.rpc.client.util.AsyncRemoteInvoke类。
4. consumer和provider之间单链路
5. 基于netty构建
6. 基于DelimiterBasedFrameDecoder解决TCP粘包问题
7. 使用目前最优秀的序列化工具google protocol buffer
8. 注册中心采用了zookeeper
9. consumer定时拉取zookeeper注册信息
10. consumer发现某条链路不可用，从zookeeper中移除该链路信息
11. 有些情况可能会导致内存泄漏。比如服务端宕机无法返回数据，那么放在MatchUtil.MAP中的数据无法被定时任务清除(ResponseWrapper.isDone() = false)。此处应做一个超时机制(做是不可能做的,虽然很简单但是狗命要紧)。

#### 项目开发周期

  2 days

#### 项目模块

项目模块说明
1. dubbo-like-rpc-client: rpc服务的客户端，提供给consumer使用
2. dubbo-like-rpc-common: 定义了client和server需要的一些通用bean和工具
3. dubbo-like-rpc-server: rpc服务的服务端，会在spring容器启动时启动
4. dubbo-like-rpc-service-util: 服务注册和发现的工具类。consumer会隔一段时间从zookeeper上拉取注册信息
5. dubbo-like-rpc-spring-boot-starter: 该rpc框架和spring boot整合的工具

#### 安装教程

1. git pull本项目到本地
2. mvn clean install本项目到本地仓库
3. 运行测试项目：https://gitee.com/ariestp/rpc-sb-test

#### 注意事项

1. 本项目部分代码使用到了java 8的特性，如lambda和stream。最低需要jdk 8.
2. 等到控制台出现pull data from zookeeper的日志时再使用rpc。否则会空指针

#### 参与贡献

1.  aries:项目主要编码、测试、设计
2.  huangtao: 项目部分编码



#### 启动流程

1. spring容器启动时为被RpcConsumer注解的接口生成动态代理 @see: com.aries.rpc.spring.starter.domain.postProcessBeforeInitialization 
    和@see: com.aries.dubbo.like.rpc.client.proxy.ServiceProxy
2. 启动RpcServer（此处会默认启动，暂时没做条件启动） @see: com.aries.rpc.spring.starter.service.helper.RpcServerConfiguration 和
    @see： com.aries.dubbo.like.rpc.server.bootstrap.RpcServerBootStrap
3. 为被RpcProvider注解的class中的被Autowired注解的field赋值 @see: com.aries.rpc.spring.starter.domain.AnnotationBean.postProcessAfterInitialization()
4. 注册本地服务信息 @see: com.aries.dubbo.like.rpc.server.bootstrap.RpcServerBootStrap 和 @see: com.aries.dubbo.like.rpc.service.util.discover.zk.ZookeeperRegistry.registry()
5. 拉取zookeeper注册信息 @see: com.aries.rpc.spring.starter.service.helper.RpcServerConfiguration 和@see: com.aries.dubbo.like.rpc.service.util.discover.zk.ZookeeperDiscovery.pullData()
6. 将被RPCProvider标记的Class名称和实例缓存到 RpcServerContext的Map<String,Object>中. @see: com.aries.rpc.spring.starter.domain.AnnotationBean.postProcessBeforeInitialization()

#### rpc流程

同步请求（从ServerProxy.class 开始阅读、随后RpcClientHandler.class和RpcServerHandler.class）:

1. 构造ServerRequest对象，初始化对象中的id、className、methodName、paramTypes、params参数
2. 将ServerRequest对象序列化成byte[]
3. 申请ByteBuf（netty的缓存组件）
4. 将ServerRequest序列化后的byte[]和包分隔符写入ByteBuf（防止粘包）
5. 将ServerRequest的id作为key，一个空的ResponseWrapper作为value放入ConcurrentHashMap中
6. 将ByteBuf通过链路发送给服务端，当前线程阻塞
7. 服务端收到客户端发出的ByteBuf包，并将其反序列化成ServerRequest对象
8. 服务端根据ServerRequest对象中的className、methodName、paramTypes参数从 启动流程6中的 RpcServerContext的Map<String,Object>中获取需要执行的方法，并使用反射机制调用方法，最后返回方法执行的返回值
9. 服务端构建ServerResponse对象，将ServerRequest的id和上一步中方法执行的返回值填充给自己。如果方法执行没报错，会将ServerResponse的code设置为200表示服务端方法调用顺利。否则将code设置为500并填充错误信息
10. 服务端将ServerResponse对象序列化成byte[],申请ByteBuf并将byte[]和粘包分隔符写入
11. 服务端将上一步中的ByteBuf通过channel发送给客户端
12. 客户端收到响应，根据响应的id从 5 中的ConcurrentHashMap获取到ResponseWrapper对象，将响应填充到ResponseWrapper中，并唤醒6中阻塞的线程
13. 如果ServerResponse的code为200，正常返回data，否则抛出异常并打印异常信息。

异步请求：

1. 大致流程和同步请求差不多，只不过consumer发起调用后会当即返回ResponseWrapper对象，consumer可以通过ResponseWrapper.ok()方法得知服务端是否响应，并调用ResponseWrapper.getResponseData()获取响应数据。
2. 如果ResponseWrapper.ok()，调用ResponseWrapper.getResponseData()不会阻塞，否则当前线程阻塞

#### 更多项目

1. aries_framework :一款实现了spring、spring mvc整合了mybatis并具有spring boot风格的框架    https://github.com/RengarS/aries_framework

2. mynettyrpc: 基于netty、google protocol buffer的spring cloud restTemplate风格的rpc框架   https://gitee.com/ariestp/mynettyrpc

