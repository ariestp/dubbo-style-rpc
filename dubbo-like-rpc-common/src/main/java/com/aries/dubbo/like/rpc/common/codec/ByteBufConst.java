package com.aries.dubbo.like.rpc.common.codec;

import lombok.Getter;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/23
 * Description:
 */
public class ByteBufConst {
    private static byte[] delimiter = "_$$".getBytes();
    //分隔符的byte[]长度

    private static final int DELIMITER_LENGTH = 3;

    public static byte[] Delimiter() {
        return delimiter;
    }

    public static int delimiterLength() {
        return DELIMITER_LENGTH;
    }
}
