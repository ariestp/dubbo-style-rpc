package com.aries.dubbo.like.rpc.common.util;

import com.aries.dubbo.like.rpc.common.annotations.RpcProvider;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Author: aries
 * Date: 2018/8/23
 * Description: 扫描包中的类
 */
@Slf4j
public class ClassUtil {
    private static String basePackage;

    public static Set<Class<?>> getRpcProviderClassSet() {
        Reflections reflections = new Reflections(basePackage);

        return reflections.getTypesAnnotatedWith(RpcProvider.class);

    }

    public static void setBasePath(String basePath) {
        basePackage = basePath;
    }

}
